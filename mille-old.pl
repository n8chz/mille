:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(http/html_write)).

/*
 * This cluster of predicates defines
 *  shuffle_deck
 * which unifies its parameter with a shuffled Mille Bornes deck.
 */

list_of(N, X, L) :- % h/t Anne Ogborn
  length(L, N),
  maplist('='(X), L).

deal_hand(Deck, Hand, Rest) :-
  Deck = [C1, C2, C3, C4, C5, C6 | Rest],
  Hand = [C1, C2, C3, C4, C5, C6].

unshuffled_deck(D) :-
  list_of(3, accident, Acc), list_of(3, out_of_gas, Oog),
  list_of(3, flat_tire, Flt), list_of(5, stop, Stp),
  list_of(4, speed_limit, Spd), list_of(6, repairs, Rep),
  list_of(6, gasoline, Gas), list_of(6, spare_tire, Spt),
  list_of(14, go, Rol), list_of(6, end_of_limit, Eol),
  list_of(1, driving_ace, Ace), list_of(1, extra_tank, Ext),
  list_of(1, puncture_proof, Ppt), list_of(1, right_of_way, Row),
  list_of(10, 25, Xxv), list_of(10, 50, L), list_of(10, 75, Lxxv),
  list_of(12, 100, C), list_of(4, 200, CC),
  F = [
    Acc, Oog, Flt, Stp, Spd, Rep, Gas, Spt, Rol, Eol, Ace, Ext, Ppt, Row, Xxv,
    L, Lxxv, C, CC
  ],
  flatten(F, D).

shuffle_deck(D) :-
  unshuffled_deck(U),
  random_permutation(U, D).

/*
 * This cluster of predicates defines
 *  initialize_game

 * a dict defining the initial game state.
 * The key /deck/ contains the state of the deck
 * after the hands have been dealt.
 * The key /players/ contains an array of player dicts,
 * each of which contains the player`s tableau state and hand.
 */

initialize_player(Name, Dict, D1, D2) :-
  deal_hand(D1, Hand, D2),
  Dict = Name{battle: stop, speed: end_of_limit, safeties: [], distance: [], hand: Hand}.

initialize_players([], [], D, D).
initialize_players(Names, Dicts, D1, D2) :-
  [Name | OtherNames] = Names,
  [Dict | OtherDicts] = Dicts,
  initialize_player(Name, Dict, D1, X),
  initialize_players(OtherNames, OtherDicts, X, D2).
  

initialize_game(PlayerNames, InitialState) :-
  shuffle_deck(D1),
  initialize_players(PlayerNames, Dicts, D1, D2),
  InitialState = game{players: Dicts, deck: D2, finish_line: 700}.

playable_cards(Game, CanPlay, CanNotPlay) :-
  [Player | _] = Game.players,
  partition(can_use_on(Player, P2), Player.hand, CanPlay, CanNotPlay),
  member(P2, Game.players).

pick_card(G, G) :-
  G.deck = [].

pick_card(G1, G2) :-
  [Player | Others] = G1.players,
  [Card | Deck] = G1.deck,
  Hand = Player.hand,
  Players = [Player.put(hand, [Card | Hand]) | Others],
  G2 = G1.put(deck, Deck).put(players, Players).

discard_one(UnplayableCards, PlayableCards, G1, G2, Message) :-
  random_select(DiscardedCard, UnplayableCards, NotDiscarded),
  [Player | OtherPlayers] = G1.players,
  is_dict(Player, PlayerName),
  append(PlayableCards, NotDiscarded, NewHand),
  G2 = G1.put(players, [Player.put(hand, NewHand) | OtherPlayers]),
  Message = [PlayerName, ' discarded ', DiscardedCard].

can_use_card_on(P1, Card, P2) :- can_use_on(P1, P2, Card). % kludge?

use_card_on(Card, P1, P2, P1after, P2after, Message) :-
  remedy_for(_, Card),
  P1 \= P2,
  select(Card, P1.hand, Hand),
  P1after = P1.put(hand, Hand),
  P2after = P2.put(battle, Card),
  is_dict(P1, P1name),
  is_dict(P2, P2name),
  Message = [P1name, ' played ', Card, ' on ', P2name].

use_card_on(Card, P1, P2, P1after, P2after, Message) :-
  remedy_for(Card, _),
  P1 = P2,
  select(Card, P1.hand, Hand),
  P1after = P1.put(hand, Hand).put(battle, Card),
  P2after = P1after,
  is_dict(P1, P1name),
  Message = [P1name, ' obtained ', Card].

use_card_on(Card, P1, P2, P1after, P2after, Message) :-
  safety_for(Card, _),
  P1 = P2,
  select(Card, P1.hand, Hand),
  Safeties = P1.safeties,
  P1after = P1.put(hand, Hand).put(safeties, [Card | Safeties]),
  P2after = P1after,
  is_dict(P1, P1name),
  Message = [P1name, ' obtained ', Card].

use_card_on(Card, P1, P2, P1after, P2after, Message) :-
  integer(Card),
  P1 = P2,
  select(Card, P1.hand, Hand),
  Distance = P1.distance,
  P1after = P1.put(hand, Hand).put(distance, [Card | Distance]),
  P2after = P1after,
  is_dict(P1, P1name),
  sum_list(P2after.distance, Miles),
  Message = [P1name, ' advanced ', Card, ' miles  to ', Miles].

use_card(_, PlayableCards, G1, G2, Message) :-
  [P1 | _] = G1.players,
  random_select(Card, PlayableCards, _),
  partition(can_use_card_on(P1, Card), G1.players, Playees, _),
  random_select(P2, Playees, _),
  use_card_on(Card, P1, P2, P1after, P2after, Message),
  select(P2, G1.players, P2after, L1),
  [_ | NotP1] = L1,
  G2 = G1.put(players, [P1after | NotP1]).

rotate_list([], []).
rotate_list([H | T], R) :-
  append(T, [H], R).

take_turn(G1, G2, Messages, [Message | Messages]) :-
  pick_card(G1, G1a),
  playable_cards(G1a, Use, Discard),
  % h/t pfctdayelise https://stackoverflow.com/a/1777891/948073
  (
      Use = [],
      discard_one(Discard, Use, G1a, G1b, Message)
    ;
      Use \= [],
      use_card(Discard, Use, G1a, G1b, Message)
  ),
  rotate_list(G1b.players, Players),
  G2 = G1b.put(players, Players).

do_race(G, G, M1, M2) :- race_over(G, M1, M2).
do_race(G1, G2, M1, M2) :-
  take_turn(G1, G, M1, M),
  do_race(G, G2, M, M2).

first_n(0, _, []).
first_n(_, [], []).
first_n(N, [L | LL], [F | FF]) :-
  M is N - 1,
  first_n(M, LL, FF),
  L = F.

:- http_handler('/', mille_bornes, []).

server(Port) :-
        http_server(http_dispatch, [port(Port)]).

% :- server(8000).

output_messages(Messages, Paragraphs) :-
  reverse(Messages, MessagesInOrder),
  convlist([X, Y] >> (Y = p(X)), MessagesInOrder, Paragraphs).

mille_bornes(_Request) :-
  Critters = [tux, xue, suzanne, amanda, wilber, puffy, sara, minix, kitty, pidgin, konqi, python, mozilla],
  random_between(2, 3, Nplayers),
  random_permutation(Critters, RandomCritters),
  first_n(Nplayers, RandomCritters, PlayerNames),
  initialize_game(PlayerNames, G1),
  do_race(G1, _, [], Messages),
  output_messages(Messages, Paragraphs),
  eject_page(Paragraphs).

eject_page(Paragraphs) :-
  reply_html_page(
    [title('An iteration of the game "Mille Bornes"')],
    Paragraphs
  ).

safety_for(right_of_way, stop).
safety_for(driving_ace, accident).
safety_for(extra_tank, out_of_gas).
safety_for(puncture_proof, flat_tire).

remedy_for(go, stop).
remedy_for(repairs, accident).
remedy_for(gasoline, out_of_gas).
remedy_for(spare_tire, flat_tire).


is_safe(X, Hazard) :-
  member(Safety, X.safeties),
  safety_for(Safety, Hazard).

 dist_to_go(G, X, D) :-
   sum_list(X.distance, DistSoFar),
   G.finish_line is DistSoFar + D.

% race_over(G) :-
%   member(Player, G.players),
%   dist_to_go(G, Player, 0).
% 
% race_over(G) :- % kludge
%   G.deck = [].

race_over(G, M1, M2) :-
  G.deck = [];
  member(Winner, G.players),
  sum_list(Winner.distance, D),
  D >= 700, % kludge
  is_dict(Winner, Name),
  M2 = [['looks like ', Name, ' won'] | M1].

could_go(X, 0) :-
  X.battle = Battle,
  remedy_for(Remedy, Battle),
  Remedy \= go.
could_go(X, 200) :-
  is_safe(X, stop),
  Battle = X.battle,
  remedy_for(Battle, _Hazard).
could_go(X, 200) :-
  X.battle = go,
  X.speed \= speed_limit.
could_go(X, 50) :-
  X.battle = go,
  \+ is_safe(X, stop),
  X.speed = speed_limit.
could_go(X, 0) :-
  X.battle \= go,
  \+ is_safe(X, stop).

% can_go(X, D) :-
%   could_go(X, CouldGo),
%   dist_to_go = 700, % kludge
%   min_list([CouldGo, DistToGo], D).

can_go(X, D) :-
  could_go(X, D). % kludge

:- use_module(library(chr)).

can_use_on(X, Y, Hazard) :-
  X \= Y,
  is_dict(X, P1name),
  is_dict(Y, P2name),
  format("~w ~w ~w~n", [P1name, P2name, Hazard]),
  member(Hazard, [stop, flat_tire, out_of_gas, accident]),
  \+ is_safe(Y, Hazard),
  can_go(Y, Speed),
  Speed > 0.
can_use_on(X, X, Safety) :-
  safety_for(Safety, _).
can_use_on(X, X, end_of_limit) :-
  X.speed = speed_limit,
  \+ is_safe(X, stop).
can_use_on(X, Y, speed_limit) :-
  X \= Y,
  Y.speed \= speed_limit,
  \+ is_safe(Y, stop).
can_use_on(X, X, Remedy) :-
  remedy_for(Remedy, Hazard),
  X.battle = Hazard,
  \+ is_safe(X, Hazard).
can_use_on(X, X, Distance) :-
  integer(Distance),
  can_go(X, Speed),
  Distance =< Speed.

