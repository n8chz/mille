/*
 * This cluster of predicates defines
 *  shuffle_deck
 * which unifies its parameter with a shuffled Mille Bornes deck.
 */

list_of(N, X, L) :- % h/t Anne Ogborn
  length(L, N),
  maplist('='(X), L).

unshuffled_deck(D) :-
  list_of(3, accident, Acc), list_of(3, out_of_gas, Oog),
  list_of(3, flat_tire, Flt), list_of(5, stop, Stp),
  list_of(4, speed_limit, Spd), list_of(6, repairs, Rep),
  list_of(6, gasoline, Gas), list_of(6, spare_tire, Spt),
  list_of(14, go, Rol), list_of(6, end_of_limit, Eol),
  list_of(1, driving_ace, Ace), list_of(1, extra_tank, Ext),
  list_of(1, puncture_proof, Ppt), list_of(1, right_of_way, Row),
  list_of(10, 25, Xxv), list_of(10, 50, L), list_of(10, 75, Lxxv),
  list_of(12, 100, C), list_of(4, 200, CC),
  F = [
    Acc, Oog, Flt, Stp, Spd, Rep, Gas, Spt, Rol, Eol, Ace, Ext, Ppt, Row, Xxv,
    L, Lxxv, C, CC
  ],
  flatten(F, D).

shuffle_deck(D) :-
  unshuffled_deck(U),
  random_permutation(U, D).

/*
 * This establishes an initial game state, represented as a dict
 */

deal_hand(Deck, Hand, RestOfDeck) :-
  Deck = [C1, C2, C3, C4, C5, C6 | RestOfDeck],
  Hand = [C1, C2, C3, C4, C5, C6].

initialize_players(Deck, [], Deck, []).
initialize_players(
  Deck,
  [PlayeName | PlayerNames],
  RestOfDeck,
  [Player | Players]
) :-
  deal_hand(Deck, Hand, RestOfDeck),
  Player = PlayerName{
    battle: [], speed: [], safeties: [], miles: [], hand: Hand
  },
  initialize_players(RestOfDeck, RestOfPlayers, Deck).

initialize_game(PlayerNames, InitialState) :-
  shuffle_deck(Deck),
  initialize_players(Deck, PlayerNames, Players),
  InitialState = game{
    player_names: PlayerNames,
    players: Players,
    deck: Deck,
    discards: [],
    finish_line: 700,
  }.

/*
 * Classification of moves as legal or otherwise
 */

can_go(Player) :-
  member(right_of_way, Player.safeties);
  [go | _] = Player.battle.

safety_for(driving_ace, accident).
safety_for(extra_tank, out_of_gas).
safety_for(puncture_proof, flat_tire).
safety_for(right_of_way, stop).

remedy_for(gasoline, out_of_gas).
remedy_for(go, stop).
remedy_for(repairs, accident).
remedy_for(spare_tire, flat_tire).

/* self plays */

can_play_on(Player, Safety, Player) :-
  safety_for(Safety, _).
can_play_on(Player, Mileage, Player) :-
  integer(Mileage),
  Mileage =< 50,
  can_go(Player).
can_play_on(Player, Mileage, Player) :-
  integer(Mileage),
  can_go_(Player),
  \+ (Player.speed = [speed_limit | _]).
can_play_on(Player, Remedy, Player) :-
  Player.battle = [Hazard | _],
  remedy_for(Remedy, Hazard).

/* battle play */

can_play_on(Player, speed_limit, Playee) :-
  \+ member(right_of_way, Playee.safeties),
  \+ (Playee.speed = [speed_limit | _]).
can_play_on(Player, Card, Playee) :-
  can_go(Playee),
  safety_for(Safety, Card),
  \+ member(Safety, Playee.safeties).

/*
 * Execution of moves
 */

update_player_list(GameState, Player, NewPlayerState, NewGameState) :-
  select(Player, GameState.players, OtherPlayers),
  [NewPlayerState | OtherPlayers] = NewPlayerList,
  GameState.put(players, NewPlayerList) = NewGameState.

coup_fourre(GameState, Player, NewGameState) :-
  member(Safety, Player.hand),
  can_play_on(Player, Safety, Player),
  [Safety | Player.safeties] = Safeties,
  Player.safeties.put(Safeties) = NewPlayerState,
  update_player_list(GameState, Player, NewPlayerState, NewGameState).

/* NOTE: this one will be recursive */
draw_card(GameState, Player, NewGameState, NewPlayerState) :-
  !. /* TODO: finish this predicate */

/*
    Brief description of simple (naive) strategy:

    Active play priorities:

    Coup fourre
    Attack opponent
    Get going
    Post mileage
    Add safety

    Discard order:

    Obsolete safeties and hazards
    Redundant safeties (except go, end_of_limit) and hazards
    Redundant go cards
    Redundant end_of_limit cards
    Lowest above-limit mileage if limited
    Lowest mileage otherwise
 */

/* TODO: define these predicates */
discard(GameState, Player, NewGameState) :- !.
play_a_card(GameState, Player, NewGameState) :- !.

playable_cards(GameState, Player, PlayableCards) :-
  member(Player, GameState.players),
  member(Playee, GameState.players),
  bagof(
    Card,
    Playee^(
      member(Card, Player.hand),
      can_play_on(Player, Card, Playee)
    ),
    PlayableCards
  ).

player_turn(GameState, Player, NewGameState) :-
  coup_fourre(GameState, Player, Intermediate); GameState = Intermediate1,
  draw_card(Intermediate1, Player, Intermediate2, NewPlayerState),
  playable_cards(Intermediate1, Player, PlayableCards),
  PlayableCards == [] -> discard(Intermediate2, Player, Intermediate3);
  play_a_card(Intermediate2, Player, Intermediate3),
  /* rotate player_names so we know whose turn is next */
  Intermediate3.player_names = [Player.name | OtherPlayerNames],
  concat([OtherPlayerNames, [Player.name]], PlayerNames),
  Intermediate3.set(player_names, PlayerNames) = NewGameState.


